from django.forms import ModelForm, DateInput, Select, SelectDateWidget, TextInput
from .models import RasprModel

class RasprForm(ModelForm):
    class Meta:
        model = RasprModel
        fields = ['raspr_date', 'district', 'department', 'organization',
                  'inn', 'performer', 'check_type', 'date_proved']
        widgets ={
            'raspr_date': DateInput(attrs={'id': 'date1', 'placeholder': 'Дата распоряжения'}),
            'district': Select(),
            'department': Select(),
            'organization': TextInput(attrs={'placeholder': 'Организация'}),
            'inn': TextInput(attrs={'placeholder': 'ИНН'}),
            'performer': Select(),
            'check_type': Select(),
            'date_proved': DateInput(attrs={'id': 'date2', 'placeholder': 'Дата проведения'}),

        }
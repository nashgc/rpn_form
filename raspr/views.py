from django.shortcuts import render
from .form import RasprForm

# Create your views here.


def raspr_form(request):
    form = RasprForm
    return render(request, 'raspr/raspr.html', {'form': form})


def raspr_form_save(request):
    RF = RasprForm
    if request.method == 'POST':
        form = RasprForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'raspr/raspr.html',
                          {'sended': 'Спасибо за уделённое время! Ваш отзыв успешно отправлен.'})
        else:
            return render(request, 'raspr/raspr.html',
                          {'error': 'Произошла ошибка, обновите страницу и попробуйте снова.'})
    return render(request, 'raspr/raspr.html', {'form': RF})

from django.contrib import admin
from .models import RasprModel

# Register your models here.


class RasprAdmin(admin.ModelAdmin):
    list_display = ('raspr_date', 'district', 'department', 'organization',
                    'inn', 'performer', 'check_type', 'date_proved')
    list_filter = ('raspr_date', 'date_proved')
    search_fields = ('inn', 'organization')
admin.site.register(RasprModel, RasprAdmin)

# Generated by Django 2.1.4 on 2018-12-22 08:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('raspr', '0002_auto_20181221_2230'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rasprmodel',
            name='check_type',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='rasprmodel',
            name='department',
            field=models.CharField(choices=[('01_Орготдел', '01_Орготдел'), ('03_ЗПП', '03_ЗПП'), ('04_Транспорт', '04_Транспорт'), ('05_Коммуналка', '05_Коммуналка'), ('06_Труд', '06_Труд'), ('07_Дети', '07_Дети'), ('08_Питание', '08_Питание'), ('09_Эпид', '09_Эпид'), ('10_Жалобы', '10_Жалобы'), ('11_Радиологи', '11_Радиологи'), ('12_Бухгалтерия', '12_Бухгалтерия'), ('15_Юристы', '15_Юристы')], max_length=30),
        ),
        migrations.AlterField(
            model_name='rasprmodel',
            name='district',
            field=models.CharField(choices=[('Московский', 'Московский'), ('Выборгский', 'Выборгский'), ('Невский', 'Невский'), ('Адмиралтейский', 'Адмиралтейский'), ('Приморский', 'Приморский'), ('Кировский', 'Кировский'), ('Управление', 'Управление')], max_length=30),
        ),
        migrations.AlterField(
            model_name='rasprmodel',
            name='performer',
            field=models.CharField(choices=[('РоманцоваВ.Л.', 'РоманцоваВ.Л.'), ('ЗаботинаИ.А.', 'ЗаботинаИ.А.')], max_length=30),
        ),
    ]

from django.db import models

# Create your models here.

class RasprModel(models.Model):

    district_list = (
        ('Московский', 'Московский'),
        ('Выборгский', 'Выборгский'),
        ('Невский', 'Невский'),
        ('Адмиралтейский', 'Адмиралтейский'),
        ('Приморский', 'Приморский'),
        ('Кировский', 'Кировский'),
        ('Управление', 'Управление'),
    )

    department_list = (
        ('01_Орготдел', '01_Орготдел'),
        ('03_ЗПП', '03_ЗПП'),
        ('04_Транспорт', '04_Транспорт'),
        ('05_Коммуналка', '05_Коммуналка'),
        ('06_Труд', '06_Труд'),
        ('07_Дети', '07_Дети'),
        ('08_Питание', '08_Питание'),
        ('09_Эпид', '09_Эпид'),
        ('10_Жалобы', '10_Жалобы'),
        ('11_Радиологи', '11_Радиологи'),
        ('12_Бухгалтерия', '12_Бухгалтерия'),
        ('15_Юристы', '15_Юристы'),
    )

    performer_list = (
        ('РоманцоваВ.Л.', 'РоманцоваВ.Л.'),
        ('ЗаботинаИ.А.', 'ЗаботинаИ.А.'),
    )

    check_type_list = (
        ('Плановая_выездная', 'Плановая_выездная'),
        ('Внеплановая_выездная', 'Внеплановая_выездная'),
        ('Внеплановая_документарная', 'Внеплановая_документарная'),
    )

    raspr_date = models.DateField()
    district = models.CharField(max_length=30, choices=district_list)
    department = models.CharField(max_length=30, choices=department_list)
    organization = models.CharField(max_length=255)
    inn = models.IntegerField()
    performer = models.CharField(max_length=30, choices=performer_list)
    check_type = models.CharField(max_length=50, choices=check_type_list)
    date_proved = models.DateField()
